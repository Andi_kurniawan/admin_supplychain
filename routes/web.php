<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WEB\AuthController;
use App\Http\Controllers\WEB\PartnerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout')
    ->name('logout');
Route::get('/home', 'HomeController@index')->name('home');


// Route::post('login', [AuthController::class, 'login'])->name('login');
Route::get('partner', [PartnerController::class, 'index'])->name('partner');
Route::get('addpartnerregistration', [PartnerController::class, 'addpartnerregistration'])->name('addpartnerregistration');
Route::post('partnerregistration', [PartnerController::class, 'add'])->name('partnerregistration');
Route::get('feedback', [PartnerController::class, 'feedback'])->name('feedback');
Route::get('viewfeedback/{id}', [PartnerController::class, 'viewfeedback'])->name('viewfeedback');
Route::get('handlefeedback/{id}', [PartnerController::class, 'handlefeedback'])->name('handlefeedback');
Route::post('savefeedback', [PartnerController::class, 'savefeedback'])->name('savefeedback');
