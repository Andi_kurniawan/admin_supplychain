@extends('layouts.app')

@section('content')

@include('layouts.datatablecss')

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class="btn-group pull-right">
                        <a href="{{ route('addpartnerregistration') }}"
                            class="btn btn-default waves-effect waves-light">
                            <span class="btn-label">
                                <i class="fa fa-plus"></i>
                            </span>
                            Add Partner
                        </a>
                    </div>
                    <p class="text-muted page-title-alt"></p>
                    <h4 class="m-t-0 header-title page-header">
                        <b>Partner</b>
                    </h4>
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Partner ID</th>
                                <th>Company Name</th>
                                <th>Corespondence Name</th>
                                <th>Corespondence Email</th>
                                <th>Corespondence Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($data as $key=>$item)
                            <tr>
                                <td scope="row" style="text-align: center">{{ $key + 1 }}</td>
                                <td>{{ $item->PartnerID }}</td>
                                <td>{{ $item->CompanyName }}</td>
                                <td>{{ $item->Correspondence_Name }}</td>
                                <td>{{ $item->Correspondence_Email }}</td>
                                <td>{{ $item->Correspondence_MobilePhone_No }}</td>
                                <td class="actions">
                                    <a :href="'/generalsetting/view/' + item.id" class="on-default edit-row">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    |
                                    <a :href="'/generalsetting/edit/' + item.id" class="on-default edit-row">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    {{-- |
                                <a :href="'/generalsetting/delete/' + item.id" class="on-default remove-row">
                                    <i class="fa fa-trash"></i>
                                </a> --}}
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- <script type="text/javascript" src="{{asset('js/app.js') }}"></script> --}}
    @include('layouts.js')
    @include('layouts.datatablejs')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
        $('#datatable-fixed-header').dataTable({
            fixedHeader: true
        });
    });
    </script>

    <script>
        $(function() {
    
            @if( Session::has('alert') )
    
            swal("Success","{{ Session::get('alert') }}",  "success" );
        
            @endif
            @if( Session::has('danger') )
    
            swal("Error","{{ Session::get('danger') }}",  "error" );
        
            @endif
        });
    </script>
    @endsection