@extends('layouts.app')

@section('content')

@include('layouts.datatablecss')

<div class="content">
    <div class="container">

    </div>

</div>
{{-- <script type="text/javascript" src="{{asset('js/app.js') }}"></script> --}}
@include('layouts.js')
@include('layouts.datatablejs')


<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable-fixed-header').dataTable({
            fixedHeader: true
        });
    });
</script>

@endsection