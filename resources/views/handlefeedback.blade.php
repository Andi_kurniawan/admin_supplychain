@extends('layouts.app')

@section('content')

<link href="{{ asset('Purple/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default waves-effect waves-light"
                            onclick="window.history.go(-1); return false;">
                            <span class="btn-label">
                                <i class="fa fa-arrow-left"></i>
                            </span>Back
                        </button>
                    </div>
                    <p class="text-muted page-title-alt"></p>
                    <h4 class="m-t-0 header-title page-header">
                        <b>Feedback Detail</b>
                    </h4>

                    {{-- <form method="POST" action="{{ route('savefeedback') }}"> --}}
                    <form class="form-horizontal" role="form" action="{{ route('savefeedback') }}" method="POST">
                        @csrf
                        @foreach ($feed as $item)
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Prospect Partner
                                            Name</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->Prospect_Partner_Name }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Company Name</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->CompanyName }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Email</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->Email }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Mobile Phone No</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->MobilePhoneNo }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Title</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->Title }}
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Notes</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->Notes }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Status</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->Status }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Handle by</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->Handleby }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-5 control-label">Handle Date</label>
                                        <div class="col-sm-1 control-label">:</div>
                                        <div class="col-sm-6 control-label" style="text-align: left">
                                            {{ $item->HandleDate }}
                                        </div>
                                    </div>
                                </div>

                            </div><br><br>

                            <input type="hidden" value=" {{ $item->id }}" name="id">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label">Handle By</label>
                                <div class="col-sm-1 control-label">:</div>
                                <div class="col-sm-6 control-label" style="text-align: left">
                                    <select name="handleby" id="" class="form-control">
                                        <option value="Baihaki Tanjung">Baihaki Tanjung</option>
                                        <option value="Tegar Wicaksono">Tegar Wicaksono</option>
                                    </select>
                                </div>
                            </div>

                            <button class="btn btn-primary waves-effect waves-light" type="submit">Submit</button>

                            <br />
                            <br />
                        </div>
                        @endforeach
                    </form>
                </div>
            </div>
        </div>

        {{-- <div id="app">
            <mitra-view :vhdview="{{json_encode($view)}}">
        <mitra-view>
    </div> --}}

</div>
</div>

{{-- <script type="text/javascript" src="{{asset('js/app.js') }}"></script> --}}
<!-- Sweet-Alert  -->
<script src="{{ asset('Purple/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('Purple/assets/plugins/parsleyjs/parsley.min.js') }}"></script>

@endsection