@extends('layouts.app')

@section('content')

@include('layouts.datatablecss')



<link rel="stylesheet" href="{{ asset('Purple/assets/plugins/morris/morris.css') }}">
<link href="{{ asset('Purple/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('Purple/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    .nvd3 text {
        font: 400 9px Arial;
    }

    .btn-primary,
    .btn-success,
    .btn-default,
    .btn-info,
    .btn-warning,
    .btn-danger,
    .btn-inverse,
    .btn-purple,
    .btn-pink {
        color: #000000 !important;
        border: 1px solid #ffffff !important;
    }

    .panel .panel-body {
        padding: 14px 0px 0px 0px;
    }

    .nav.nav-tabs+.tab-content {
        margin-bottom: 0px;
        padding: 10px;
        min-height: 505px;
    }
</style>

<div class="content">
    <div class="container">
        <loading :show="loading.show" :label="loading.label"></loading>
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <div class="btn-group pull-right">
                        <button type="button" v-on:click="kembalikePC" class="btn btn-default waves-effect waves-light">
                            <span class="btn-label">
                                <i class="fa fa-arrow-left"></i>
                            </span>Back
                        </button>
                    </div>
                    <p class="text-muted page-title-alt"></p>
                    <h4 class="m-t-0 header-title page-header">
                        <b>Partner Registration</b>
                    </h4>
                    <form method="POST" enctype='multipart/form-data' action="{{ route('partnerregistration') }}">
                        @csrf
                        <div class="col-md-6">
                            {{-- <div class="form-group">
                                <label for="userName">PartnerID*</label>
                                <input type="text" name="PartnerID" parsley-trigger="change" required
                                    placeholder="Enter Partner ID" class="form-control" id="userName" />
                            </div> --}}
                            <div class="form-group">
                                <label for="userName">CompanyName*</label>
                                <input type="text" name="CompanyName" parsley-trigger="change" required
                                    placeholder="Enter Company Name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">EstablishDate*</label>
                                <input type="date" name="EstablishDate" parsley-trigger="change" required
                                    placeholder="Enter user name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Company_NIB_No*</label>
                                <input type="text" name="Company_NIB_No" parsley-trigger="change" required
                                    placeholder="Enter Company NIB No" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Company_NPWP_No*</label>
                                <input type="text" name="Company_NPWP_No" parsley-trigger="change" required
                                    placeholder="Enter Company NPWP No" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Company_ID_No*</label>
                                <input type="text" name="Company_ID_No" parsley-trigger="change" required
                                    placeholder="Enter Company ID No" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">CompanyAddress*</label>
                                <input type="text" name="CompanyAddress" parsley-trigger="change" required
                                    placeholder="Enter Company Address" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">AccountNo*</label>
                                <input type="text" name="AccountNo" parsley-trigger="change" required
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    placeholder="Enter Account No" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">AccountName*</label>
                                <input type="text" name="AccountName" parsley-trigger="change" required
                                    placeholder="Enter Account Name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">BankName*</label>
                                <select name="BankName" id="" name="BankName" class="form-control">
                                    <option value="">BCA</option>
                                    <option value="">BRI</option>
                                    <option value="">MANDIRI</option>
                                </select>
                                {{-- <input type="text" name="BankName" parsley-trigger="change" required
                                    placeholder="Enter Bank Name" class="form-control" id="userName" /> --}}
                            </div>
                            <div class="form-group">
                                <label for="userName">AgreementNo*</label>
                                <input type="text" name="AgreementNo" parsley-trigger="change" required
                                    placeholder="Enter Agreement No" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Agreement_Start_From*</label>
                                <input type="date" name="Agreement_Start_From" parsley-trigger="change" required
                                    placeholder="Enter Agreement Start From" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Agreement_End_To*</label>
                                <input type="date" name="Agreement_End_To" parsley-trigger="change" required
                                    placeholder="Enter Agreement End To" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Correspondence_Name*</label>
                                <input type="text" name="Correspondence_Name" parsley-trigger="change" required
                                    placeholder="Enter Correspondence Name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Correspondence_Title*</label>
                                <input type="text" name="Correspondence_Title" parsley-trigger="change" required
                                    placeholder="Enter Correspondence Title" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Correspondence_MobilePhone_No*</label>
                                <input type="text" name="Correspondence_MobilePhone_No" parsley-trigger="change"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57' required
                                    placeholder="Enter Correspondence MobilePhone No" class="form-control"
                                    id="userName" />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="userName">Correspondence_Email*</label>
                                <input type="text" name="Correspondence_Email" parsley-trigger="change" required
                                    placeholder="Enter Correspondence Email" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Agreement_Signer_Name*</label>
                                <input type="text" name="Agreement_Signer_Name" parsley-trigger="change" required
                                    placeholder="Enter Agreement Signer Name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Agreement_Signer_Title*</label>
                                <input type="text" name="Agreement_Signer_Title" parsley-trigger="change" required
                                    placeholder="Enter Agreement Signer Title" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Agreement_Signer_Email*</label>
                                <input type="text" name="Agreement_Signer_Email" parsley-trigger="change" required
                                    placeholder="Enter Agreement Signer Email" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Partner_Grade*</label>
                                <input type="text" name="Partner_Grade" parsley-trigger="change" required
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    placeholder="Enter Partner Grade" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">ProductID*</label>
                                <select id="" name="ProductID" class="form-control">
                                    @foreach ($product as $item)
                                    <option value="{{ $item->ProductID }}">{{ $item->ProductName }}</option>
                                    @endforeach
                                    {{-- <option value="">BCA</option>
                                    <option value="">BRI</option>
                                    <option value="">MANDIRI</option> --}}
                                </select>
                                {{-- <input type="text" name="ProductID" parsley-trigger="change" required
                                    placeholder="Enter Product ID" class="form-control" id="userName" /> --}}
                            </div>
                            <div class="form-group">
                                <label for="userName">CommisionRate*</label>
                                <input type="text" name="CommisionRate" parsley-trigger="change" required
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    placeholder="Enter Commision Rate" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">CommisionRate_Discount*</label>
                                <input type="text" name="CommisionRate_Discount" parsley-trigger="change" required
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    placeholder="Enter Commision Rate Discount" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">CommisionRate_Discount_After*</label>
                                <input type="text" name="CommisionRate_Discount_After" parsley-trigger="change" required
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    placeholder="Enter CommisionRate Discount After" class="form-control"
                                    id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Images_Bank_Cover*</label>
                                <input type="file" name="Images_Bank_Cover" parsley-trigger="change" required
                                    placeholder="Enter user name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Images_NPWP*</label>
                                <input type="file" name="Images_NPWP" parsley-trigger="change" required
                                    placeholder="Enter user name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Images_NIB*</label>
                                <input type="file" name="Images_NIB" parsley-trigger="change" required
                                    placeholder="Enter user name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Images_Agreement*</label>
                                <input type="file" name="Images_Agreement" parsley-trigger="change" required
                                    placeholder="Enter user name" class="form-control" id="userName" />
                            </div>
                            <div class="form-group">
                                <label for="userName">Is_PPN*</label>
                                <select name="Is_PPN" id="" class="form-control">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                {{-- <input type="text" name="Is_PPN" parsley-trigger="change" required
                                    placeholder="Enter Is PPN" class="form-control" id="userName" /> --}}
                            </div>
                            <div class="form-group">
                                <label for="userName">Is_PPH*</label>
                                <select name="Is_PPH" id="" class="form-control">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                {{-- <input type="text" name="Is_PPH" parsley-trigger="change" required
                                    placeholder="Enter Is PPH" class="form-control" id="userName" /> --}}
                            </div>
                            <div class="form-group">
                                <label for="userName">OrderUploadBy*</label>
                                <select name="OrderUploadBy" id="" class="form-control">
                                    <option value="API">API</option>
                                    <option value="Excel">Upload Manual/Excel</option>
                                </select>
                                {{-- <input type="text" name="OrderUploadBy" parsley-trigger="change" required
                                    placeholder="Enter OrderUpload By" class="form-control" id="userName" /> --}}
                            </div>
                        </div>
                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">Submit</button>
                            <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- /Portlet -->
<!-- End row-->


<!-- content -->

<footer class="footer text-right">
    © 2016. All rights reserved.
</footer>

</div>



</div> <!-- container -->


{{-- <script type=" text/javascript" src="{{ asset('js/app.js') }}">
</script> --}}

@include('layouts.js')


<!-- jQuery  -->
<script src="{{ asset('Purple/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/detect.js') }}"></script>
<script src="{{ asset('Purple/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('Purple/assets/js/waves.js') }}"></script>
<script src="{{ asset('Purple/assets/js/wow.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.scrollTo.min.js') }}"></script>


<script src="{{ asset('Purple/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.app.js') }}"></script>

<!--Morris Chart-->
<script src="{{ asset('Purple/assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/raphael/raphael-min.js') }}"></script>
<script src="{{ asset('Purple/assets/pages/morris.init.js') }}"></script>




<!-- jQuery  -->
<script src="{{ asset('Purple/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/detect.js') }}"></script>
<script src="{{ asset('Purple/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('Purple/assets/js/waves.js') }}"></script>
<script src="{{ asset('Purple/assets/js/wow.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.scrollTo.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/peity/jquery.peity.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>


<script src="{{ asset('Purple/assets/plugins/d3/d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/nvd3/nv.d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/pages/jquery.nvd3.init.js') }}"></script>

<script src="{{ asset('Purple/assets/pages/jquery.dashboard_3.js') }}"></script>

<script src="{{ asset('Purple/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.app.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/moment/moment.js') }}"></script>


{{-- <script src="{{ asset('Purple/assets/plugins/morris/morris.min.js') }}"></script> --}}
<script src="{{ asset('Purple/assets/plugins/raphael/raphael-min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

<!-- Todojs  -->
<script src="{{ asset('Purple/assets/pages/jquery.todo.js') }}"></script>
<!--Chartist Chart-->
<script src="{{ asset('Purple/assets/plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/chartist/js/chartist-plugin-tooltip.min.js') }}"></script>
<script src="{{ asset('Purple/assets/pages/jquery.chartist.init.js') }}"></script>



<script src="{{ asset('Purple/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.app.js') }}"></script>

<script src="{{ asset('Purple/assets/pages/jquery.dashboard_2.js') }}"></script>



<script src="{{ asset('Purple/assets/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>


<!-- Counterup  -->
<script src="{{ asset('Purple/assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/raphael/raphael-min.js') }}"></script>


<script src="{{ asset('Purple/assets/plugins/jquery-knob/jquery.knob.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/d3/d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/nvd3/nv.d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/jquery-circliful/js/jquery.circliful.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript">

</script>

<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.time.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.selection.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.orderBars.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.crosshair.js') }}"></script>
<script src="{{ asset('Purple/assets/pages/jquery.flot.init.js') }}"></script>

@include('layouts.datatablejs')

@endsection