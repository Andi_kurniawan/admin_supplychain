@extends('layouts.app')

@section('content')

@include('layouts.datatablecss')

<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <p class="text-muted page-title-alt"></p>
                    <h4 class="m-t-0 header-title page-header">
                        <b>Partner Feedback Form</b>
                    </h4>
                    <table id="datatable-fixed-header" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Prospect Partner Name</th>
                                <th>Company Name</th>
                                <th>Email</th>
                                <th>Mobile Phone</th>
                                <th>Status</th>
                                <th>Handle by</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($feed as $key=>$item)
                            <tr>
                                <td scope="row" style="text-align: center">{{ $key + 1 }}</td>
                                <td>{{ $item->Prospect_Partner_Name }}</td>
                                <td>{{ $item->CompanyName }}</td>
                                <td>{{ $item->Email }}</td>
                                <td>{{ $item->MobilePhoneNo }}</td>
                                <td>{{ $item->Status }}</td>
                                <td>{{ $item->Handleby }}</td>
                                <td class="actions">
                                    <a href="viewfeedback/{{ $item->id}}" class="on-default edit-row">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    |
                                    <a href="handlefeedback/{{ $item->id}}" class="on-default edit-row">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    {{-- |
                                <a :href="'/generalsetting/delete/' + item.id" class="on-default remove-row">
                                    <i class="fa fa-trash"></i>
                                </a> --}}
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- <script type="text/javascript" src="{{asset('js/app.js') }}"></script> --}}
    @include('layouts.js')
    @include('layouts.datatablejs')


    <script type="text/javascript">
        $(document).ready(function() {
        $('#datatable-fixed-header').dataTable({
            fixedHeader: true
        });
    });
    </script>

    @endsection