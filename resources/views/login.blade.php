<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Login</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- Icons font CSS-->
    <link href="{{ asset('colorlib-regform-3/vendor/mdi-font/css/material-design-iconic-font.min.css')}}"
        rel="stylesheet" media="all">
    <link href="{{ asset('colorlib-regform-3/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet"
        media="all">
    <!-- Font special for pages-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="{{ asset('colorlib-regform-3/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{ asset('colorlib-regform-3/vendor/datepicker/daterangepicker.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('colorlib-regform-3/css/main.css')}}" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins">
        <div class="wrapper wrapper--w780">
            <div class="card card-3">
                <div class="card-body">
                    <h2 style="font-weight: bold">Masuk</h2>
                    <small>Masuk untuk memantau Kendaraan anda</small>
                    <br><br><br>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Alamat Email</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp">
                            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small> --}}
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                        </div>
                        {{-- <img src="{{captcha_src('flat')}}" onclick="this.src='/captcha/flat?'+Math.random()"
                        id="captchaCode" alt="" class="captcha">
                        <p></p><br>
                        <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"
                            autocomplete="off"> --}}
                        {{-- <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div> --}}

                        {{-- <div class="input-group">
                            <input class="input--style-3" type="text" placeholder="Name" name="name">
                        </div>
                        <div class="input-group">
                            <input class="input--style-3 js-datepicker" type="text" placeholder="Birthdate"
                                name="birthday">
                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                        </div>
                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select name="gender">
                                    <option disabled="disabled" selected="selected">Gender</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                    <option>Other</option>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                        <div class="input-group">
                            <input class="input--style-3" type="email" placeholder="Email" name="email">
                        </div>
                        <div class="input-group">
                            <input class="input--style-3" type="text" placeholder="Phone" name="phone">
                        </div> --}}
                        <div class="p-t-10">

                            <button class="btn btn--pill btn--green" type="submit">
                                Masuk
                            </button>
                            {{-- <a href="" class="btn btn--pill btn--green" type="submit">Masuk</a> --}}
                        </div><br>
                        {{-- Lupa Password? <span style="color: #57b846">Klik disini</span> --}}
                    </form>
                </div>
                <div class="card-heading"></div>

            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="{{ asset('colorlib-regform-3/vendor/jquery/jquery.min.js')}}"></script>
    <!-- Vendor JS-->
    <script src="{{ asset('colorlib-regform-3/vendor/select2/select2.min.js')}}"></script>
    <script src="{{ asset('colorlib-regform-3/vendor/datepicker/moment.min.js')}}"></script>
    <script src="{{ asset('colorlib-regform-3/vendor/datepicker/daterangepicker.js')}}"></script>

    <!-- Main JS-->
    <script src="{{ asset('colorlib-regform-3/js/global.js')}}"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->