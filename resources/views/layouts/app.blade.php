<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
</head>
<style>
    body {
        font-size: 12px;
    }
</style>

<body class="fixed-leftwidescreen fixed-left-void">

    <!-- Begin page class="enlarged forced"-->
    <div id="wrapper" class="enlarged forced">

        <!-- Top Bar Start -->
        @include('layouts.TopBar')
        <!-- Top Bar End -->
        <br><br>
        <!-- ========== Left Sidebar Start ========== -->
        @include('layouts.LeftSideBar')
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            @yield('content')
            <!-- content -->

            <footer class="footer text-right">
                © SITAMA2020. All rights reserved.
            </footer>

        </div>

    </div>
    <!-- END wrapper -->
    {{-- @include('layouts.js') --}}
    <script src="{{ asset('Purple/assets/js/jquery.core.js') }}"></script>
    <script src="{{ asset('Purple/assets/js/jquery.app.js') }}"></script>

</body>

</html>