<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="shortcut icon" href="{{ asset('Purple/assets/images/Logo-SIP-RPM.png') }}">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Admin Vehiclo</title>

<!--Morris Chart CSS -->
{{-- <link rel="stylesheet" href="assets/plugins/morris/morris.css"> --}}

<link href="{{ asset('Purple/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
{{-- <link href="{{ asset('Purple/assets/css/pages.css') }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ asset('Purple/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="{{ asset('Purple/assets/js/modernizr.min.js') }}"></script>