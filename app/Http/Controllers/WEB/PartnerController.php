<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;

class PartnerController extends Controller
{
    public function index()
    {
        $data = DB::Select("select * from supplychain.Partner;");
        return view('partner', ['data' => $data]);
    }
    public function addpartnerregistration()
    {
        $product = DB::Select("SELECT * FROM supplychain.Product");
        return view('addpartnerregistration', ['product' => $product]);
    }
    public function feedback()
    {
        $feed = DB::Select("SELECT * FROM supplychain.Partner_Feedback_Form");
        return view('feedback', ['feed' => $feed]);
    }
    public function viewfeedback($id)
    {

        $feed = DB::Select("SELECT * FROM supplychain.Partner_Feedback_Form where id = $id");
        // dd($feed);
        return view('viewfeedback', ['feed' => $feed]);
    }

    public function handlefeedback($id)
    {
        $feed = DB::Select("SELECT * FROM supplychain.Partner_Feedback_Form where id = $id");
        // dd($feed);
        return view('handlefeedback', ['feed' => $feed]);
    }

    public function savefeedback(request $request)
    {
        $id = $request['id'];
        $handleby = $request['handleby'];
        $handledate = date('Y-m-d H:i:s');
        $status = 'Handle';

        DB::update("UPDATE supplychain.Partner_Feedback_Form
        SET Handleby = '$handleby', HandleDate = '$handledate', Status = '$status'
        WHERE id = $id");
        // $feed = DB::Select("SELECT * FROM supplychain.Partner_Feedback_Form where id = $id");
        // dd($feed);
        return redirect('feedback');
        // return view('feedback');
    }

    public function add(request $request)
    {
        $query = DB::SELECT("SELECT max(id)+1 as maxid FROM supplychain.Partner");
        // dd($query);
        foreach ($query as $i) {
            if ($i->maxid == null) {
                $mx = 1;
            } else {
                $mx = $i->maxid;
            }
        }

        $OrderNo = "P" . sprintf("%04s", $mx);
        // $PartnerID = $request['PartnerID'];

        $PartnerID = $OrderNo;
        $CompanyName = $request['CompanyName'];
        $EstablishDate = $request['EstablishDate'];
        $Company_NIB_No = $request['Company_NIB_No'];
        $Company_NPWP_No = $request['Company_NPWP_No'];
        $Company_ID_No = $request['Company_ID_No'];
        $CompanyAddress = $request['CompanyAddress'];
        $AccountNo = $request['AccountNo'];
        $AccountName = $request['AccountName'];
        $BankName = $request['BankName'];
        $AgreementNo = $request['AgreementNo'];
        $Agreement_Start_From = $request['Agreement_Start_From'];
        $Agreement_End_To = $request['Agreement_End_To'];
        $Correspondence_Name = $request['Correspondence_Name'];
        $Correspondence_Title = $request['Correspondence_Title'];
        $Correspondence_MobilePhone_No = $request['Correspondence_MobilePhone_No'];
        $Correspondence_Email = $request['Correspondence_Email'];
        $Agreement_Signer_Name = $request['Agreement_Signer_Name'];
        $Agreement_Signer_Title = $request['Agreement_Signer_Title'];
        $Agreement_Signer_Email = $request['Agreement_Signer_Email'];
        $Partner_Grade = $request['Partner_Grade'];
        $ProductID = $request['ProductID'];

        // $product = DB::Select("SELECT * FROM supplychain.Product where ProductID = '$ProductID'");
        // foreach ($product as $item) {
        //     $comisrat = $item->CommisionRate;
        //     $CommisRatDiscount = $item->CommisionRate_Discount;
        // }
        $CommisionRate = $request['CommisionRate'];
        $CommisionRate_Discount = $request['CommisionRate_Discount'];

        // $CommisionRate = $comisrat;
        // $CommisionRate_Discount = $CommisRatDiscount;

        $CommisionRate_Discount_After = $request['CommisionRate_Discount_After'];
        $Images_Bank_Cover = $request->file('Images_Bank_Cover');
        $Images_Bank_Cover                = str_replace(' ', '+', $Images_Bank_Cover);
        $Images_Bank_CoverName            = time() . '_' . uniqid() . '.' . 'png';
        Storage::disk('sftp')->put('/Vehiclo/Register/' . $Images_Bank_CoverName, file_get_contents($Images_Bank_Cover));

        $Images_NPWP = $request->file('Images_NPWP');
        $Images_NPWP                = str_replace(' ', '+', $Images_NPWP);
        $Images_NPWPName            = time() . '_' . uniqid() . '.' . 'png';
        Storage::disk('sftp')->put('/Vehiclo/Register/' . $Images_NPWPName, file_get_contents($Images_NPWP));

        $Images_NIB = $request->file('Images_NIB');
        $Images_NIB                = str_replace(' ', '+', $Images_NIB);
        $Images_NIBName            = time() . '_' . uniqid() . '.' . 'png';
        Storage::disk('sftp')->put('/Vehiclo/Register/' . $Images_NIBName, file_get_contents($Images_NIB));

        $Images_Agreement = $request->file('Images_Agreement');
        $Images_Agreement                = str_replace(' ', '+', $Images_Agreement);
        $Images_AgreementName            = time() . '_' . uniqid() . '.' . 'png';
        Storage::disk('sftp')->put('/Vehiclo/Register/' . $Images_AgreementName, file_get_contents($Images_Agreement));

        $Is_PPN = $request['Is_PPN'];
        $Is_PPH = $request['Is_PPH'];
        $Status = '1';
        $OrderUploadBy = $request['OrderUploadBy'];
        $Created_at = date('Y-m-d H:i:s');
        $Updated_at = date('Y-m-d H:i:s');

        DB::Insert("INSERT INTO `supplychain`.`Partner`
        (
        `PartnerID`,
        `CompanyName`,
        `EstablishDate`,
        `Company_NIB_No`,
        `Company_NPWP_No`,
        `Company_ID_No`,
        `CompanyAddress`,
        `AccountNo`,
        `AccountName`,
        `BankName`,
        `AgreementNo`,
        `Agreement_Start_From`,
        `Agreement_End_To`,
        `Correspondence_Name`,
        `Correspondence_Title`,
        `Correspondence_MobilePhone_No`,
        `Correspondence_Email`,
        `Agreement_Signer_Name`,
        `Agreement_Signer_Title`,
        `Agreement_Signer_Email`,
        `Partner_Grade`,
        `ProductID`,
        `CommisionRate`,
        `CommisionRate_Discount`,
        `CommisionRate_Discount_After`,
        `Images_Bank_Cover`,
        `Images_NPWP`,
        `Images_NIB`,
        `Images_Agreement`,
        `Is_PPN`,
        `Is_PPH`,
        `Status`,
        `OrderUploadBy`,
        `Created_at`,
        `Updated_at`)
        VALUES
        (
        '$PartnerID',
        '$CompanyName',
        '$EstablishDate',
        '$Company_NIB_No',
        '$Company_NPWP_No',
        '$Company_ID_No',
        '$CompanyAddress',
        '$AccountNo',
        '$AccountName',
        '$BankName',
        '$AgreementNo',
        '$Agreement_Start_From',
        '$Agreement_End_To',
        '$Correspondence_Name',
        '$Correspondence_Title',
        '$Correspondence_MobilePhone_No',
        '$Correspondence_Email',
        '$Agreement_Signer_Name',
        '$Agreement_Signer_Title',
        '$Agreement_Signer_Email',
        '$Partner_Grade',
        '$ProductID',
        '$CommisionRate',
        '$CommisionRate_Discount',
        '$CommisionRate_Discount_After',
        '$Images_Bank_CoverName',
        '$Images_NPWPName',
        '$Images_NIBName',
        '$Images_AgreementName',
        '$Is_PPN',
        '$Is_PPH',
        '$Status',
        '$OrderUploadBy',
        '$Created_at',
        '$Updated_at')");

        $baru = bcrypt("supplychain2020");
        DB::Insert("INSERT INTO `supplychain`.`users`
        (
        `PartnerID`,
        `name`,
        `email`,
        `password`,
        `created_at`,
        `updated_at`)
        VALUES
        (
        '$PartnerID',
        '$Correspondence_Name',
        '$Correspondence_Email',
        '$baru',
        '$Created_at',
        '$Updated_at')");


        session()->flash('alert', ('Data Berhasil di simpan'));
        return redirect('partner');
    }
}
