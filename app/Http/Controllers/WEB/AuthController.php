<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'email' => 'required|string|email|max:255',
        //     'password' => 'required|string|min:8',
        // ]);

        // if ($validator->fails()) {

        //     return response()->json(
        //         ['error' => $validator->errors()->all(), 'authUser' => false, 'code' => '422'],
        //         422
        //     );
        // }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://localhost:8005/api/adminsupplychain/auth/login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('email' => 'kurniawan.andi09@gmail.com', 'password' => 'supplychain2020'),
            CURLOPT_HTTPHEADER => array(
                'Cookie: _token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYmY4YmU2YzY4NDMzYjc5NGQ3ZDk3YTU5Mzk5ZmU1YzE3ZTY4YmE4OWNjMzRkYTQ1ODU5OGY1MWM2OWVkOWI0OGEzYjRkYzI3ZDRmZDJlYzciLCJpYXQiOjE2MDYxMTk2NzMsIm5iZiI6MTYwNjExOTY3MywiZXhwIjoxNjM3NjU1NjczLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.mQqWGRyZb0vFVBhe828EjqJTSYeasJIDnW37tsX_LAp2-l7d1dvHBTY0UTih3t7JQe9F8vBwW5nAOlE54lkrLGrAAavZFKz10sVCT7T10GOtQcvlUL_YAhh214ODJzc4D7zaZcwL7KK-Nq8BtPuzYaroX2vdYE9ZadWUww0nsj_3natJvOuWnf1nvuVS94_GSybU882wm6M-Dfu1T7NLyjxx4dEB3nH--NfdvKsH2Ce4s1bCv5nuhp5F5LiX_oevZFiQkCRP2h-S8uaCKL9iG78vnLp47DlUzkQqL72Uy8V6K7u4UxiwivvSfv7Ic1mFrB1dvS4Qtvz7g9MRCeAyF68hS37bxg3o43kF9IiRJztSmBr8ZkiE7pcqPCm-n15FAPj-zPMy9OdCLXlYHnJCnS-F3axNLgV-tcr0kMcjAqUszJMNF2CLVWGsVVwWpgoe7_HC8qpebu8CNlS3Z8M-FW2seHRD1hM8DC_32yDObl3mYn8FTRR2mGnsBpzw_uCdwuzLqoHYhY-TWviE78DeFvLCkptz5UA04U3c70Luu2e9Cd3jRSv7RuJmwx2K_g2a3TIGisNOB7jZwGBebAx6t198tHJXLvtbRcezRp1shCD46xjym9gQlcCQr9MJF9g0V6oiykn03FyrPh0OVFA04xQ7UAzUxbVJSOUPL4rrAMQ'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}
