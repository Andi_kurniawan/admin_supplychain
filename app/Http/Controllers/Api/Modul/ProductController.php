<?php

namespace App\Http\Controllers\API\Modul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function contactus()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $validator = Validator::make($request->all(), [
            'ProductID'                         => 'required|string',
            'ProductName'                       => 'required|string',
            'Commision_Rate'                    => 'required|string|email',
            'AssetType'                         => 'required|string',
            'Maximum_Commision_Rate_Discount'   => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['error' => $validator->errors()->all(), 'authUser' => false, 'code' => '422'],
                422
            );
        }

        if ($request) {
            // get request
            $ProductID                          = $request['ProductID'];
            $ProductName                        = $request['ProductName'];
            $Commision_Rate                     = $request['Commision_Rate'];
            $AssetType                          = $request['AssetType'];
            $Maximum_Commision_Rate_Discount    = $request['Maximum_Commision_Rate_Discount'];
            $Status                             = 'Active';
            $Date                               = Carbon::now()->format('Y-m-d H:i:s');

            DB::Insert("INSERT INTO `supplychain`.`Product`
            (
            `ProductID`,
            `ProductName`,
            `AssetType`,
            `Commision_Rate`,
            `Maximum_Commision_Rate_Discount`,
            `Status`,
            `Created_at`,
            `Updated_at`)
            VALUES
            ('$ProductID',
            '$ProductName',
            '$AssetType',
            '$Commision_Rate',
            '$Maximum_Commision_Rate_Discount',
            '$Status',
            '$Date',
            '$Date')");
        }

        return response()->json(
            ['isValid' => true, 'ResponseCode' => '200', 'ResponseDescription' => 'Data Berhasil Disimpan'],
            200
        );
    }
}
