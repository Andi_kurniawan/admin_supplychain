<?php

namespace App\Http\Controllers\Api\Modul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
// use Validator;

class PagesController extends Controller
{
    public function contactus()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $validator = Validator::make($request, [
            'Prospect_Partner_Name'  => 'required|string',
            'CompanyName'            => 'required|string',
            'Email'                  => 'required|string|email',
            'MobilePhoneNo'          => 'required|string',
            'Title'                  => 'required|string',
            'Notes'                  => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        if ($request) {
            // get request
            $Prospect_Partner_Name  = $request['Prospect_Partner_Name'];
            $CompanyName            = $request['CompanyName'];
            $Email                  = $request['Email'];
            $MobilePhoneNo          = $request['MobilePhoneNo'];
            $Title                  = $request['Title'];
            $Notes                  = $request['Notes'];
            $Status                 = 'NEW';
            $Date                   = Carbon::now()->format('Y-m-d H:i:s');

            DB::Insert("INSERT INTO `supplychain`.`Partner_Feedback_Form`
            (`Prospect_Partner_Name`,
            `CompanyName`,
            `Email`,
            `MobilePhoneNo`,
            `Title`,
            `Notes`,
            `Status`,
            `SentDate`)
            VALUES
            ('$Prospect_Partner_Name',
            '$CompanyName',
            '$Email',
            '$MobilePhoneNo',
            '$Title',
            '$Notes',
            '$Status',
            '$Date')");
        }

        return response()->json(
            ['isValid' => true, 'code' => '200', 'Message' => 'Data Berhasil Disimpan'],
            200
        );
    }

    public function getcontactus()
    { // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        // get request
        $data = DB::Select("select * from `supplychain`.`Partner_Feedback_Form` where Status = 'NEW'");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function updatecontactstatus()
    { }
}
