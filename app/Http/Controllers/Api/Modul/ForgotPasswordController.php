<?php

namespace App\Http\Controllers\Api\Modul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    public function getToken_Forgotpassword()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        $validator = Validator::make($request, [
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $email        = $request['email'];
        $date          = Carbon::now()->format('Y-m-d H:i:s');

        $cek_user = DB::select("SELECT * FROM supplychain.users  WHERE email  = '$email';");
        if (empty($cek_user)) {

            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => 'Email not found'],
                422
            );
        }

        $cek_phone = DB::select("SELECT Correspondence_MobilePhone_No,user.id as userid FROM supplychain.users user inner join supplychain.Partner partner on partner.PartnerID = user.PartnerID WHERE user.email  =  '$email';");
        foreach ($cek_phone as $item_phone) {
            $hasilphone = $item_phone->Correspondence_MobilePhone_No;
            $id_user = $item_phone->userid;
        }

        if ($hasilphone === null || $hasilphone === 0 || $hasilphone === "0" || $hasilphone === "") {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => 'Phone Number is not registered'],
                422
            );
        }

        if (!preg_match('/[^+0-9]/', trim($hasilphone)))
        // cek apakah no hp mengandung karakter + dan 0-9
        {
            if (substr(trim($hasilphone), 0, 3) == '+62')
            // cek apakah no hp karakter 1-3 adalah +62
            {
                $notelp = trim($hasilphone);
            } elseif (substr(trim($hasilphone), 0, 1) == '0')
            // cek apakah no hp karakter 1 adalah 0
            {
                $notelp = '+62' . substr(trim($hasilphone), 1);
            }
            // fungsi trim() untuk menghilangan
            // spasi yang ada didepan/belakang
        } else {
            $notelp = $hasilphone;
        }

        $otp = mt_rand(100000, 999999);
        // $cekid = DB::select("SELECT id FROM supplychain.users  WHERE email  = '$email';");
        // foreach ($cekid as $item) {
        //     $id_user = $item->id;
        // }

        DB::insert("insert into supplychain.trTokenHistory (UserID, TokenType, TokenNo, TimeStamps, Created_at, Update_at) values ('$id_user','Forgot Password','$otp','$date','$date','$date')");

        // Api send sms
        $curltoken = curl_init();

        curl_setopt_array($curltoken, array(
            CURLOPT_URL => "https://smsgw.sitama.co.id/api/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"Username\": \"RPM2020\",\n    \"password\": \"RPM@2020\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $result = curl_exec($curltoken);

        curl_close($curltoken);

        $token = json_decode($result, true)['access_token'];
        // echo $response;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://smsgw.sitama.co.id/api/SMS/smssitama",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"notelp\": \"$notelp\",\n    \"textsms\": \"Supply Chain%0AYour OTP Code is $otp\",\n    \"desc\": \"SupplyChain\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $token,
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        //end api sms

        return response()->json(
            ['id_user' => $id_user, 'isValid' => true, 'code' => '200', 'message' => 'OTP Succesfully Send'],
            200
        );
    }

    public function ValidateToken_Forgotpassword()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        $id        = $request['id'];
        $TokenNo   = $request['TokenNo'];

        $cek_user = DB::select("SELECT * FROM supplychain.users where id = '$id'");
        if (empty($cek_user)) {

            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => 'user not found'],
                422
            );
        }

        //cek otp
        $cekotp = DB::select("SELECT * FROM supplychain.trTokenHistory where UserID = '$id' and TokenType = 'Forgot Password'  ORDER BY trTokenHistory.id DESC LIMIT 1");
        foreach ($cekotp as $item) {
            $item2 = $item->TokenNo;
        }

        if ($item2 != $TokenNo) {

            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => 'OTP is not match'],
                422
            );
        }

        return response()->json(
            ['isValid' => true, 'code' => '200', 'message' => 'OTP is match'],
            200
        );
    }

    public function changepassword()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        $validator = Validator::make($request, [
            'Password' => 'required|string|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $id                 = $request['id'];
        $Password           = $request['Password'];
        $Password_Confirm   = $request['Password_Confirm'];
        $date               = Carbon::now()->format('Y-m-d H:i:s');

        $cek_user = DB::select("SELECT * FROM supplychain.users where id = '$id'");
        if (empty($cek_user)) {

            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => 'user not found'],
                422
            );
        }

        if ($Password != $Password_Confirm) {

            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => 'new password and password confirmation do not match'],
                422
            );
        }
        $baru = bcrypt($Password_Confirm);
        DB::update("UPDATE supplychain.users SET password = '$baru', Updated_at = '$date' where id = '$id'");
        // DB::insert("insert into rpm_live.RPM_Log (LoginID,LogHeader,LogDesc,LogDate,created_at,updated_at) values ('$LoginID','Change Password','Changed Password Succesfully','$date','$date','$date')");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'message' => 'Your password has been changed successfully'],
            200
        );
    }
}
